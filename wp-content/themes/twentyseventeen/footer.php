<div class="col-md-12 padding80" style="text-align:center;padding-bottom:20px !important;">
            <div class="col-md-12"><img class="" width="50" height="50"
                                        src="/wordpress/wp-content/themes/twentyseventeen/assets/images/finnkino-logo.png"
                                        alt="finnkino-logo"
                                        style="margin-bottom:30px;margin-top:50px;"></div>
            <a class="navlinks" href="https://www.finnkino.fi/" target="_blank">FINNKINO.FI</a> - <a class="navlinks"
                                                                                                     href="<?php echo get_template_directory_uri(); ?>/privacypolicy"
                                                                                                     target="_blank">REKISTERISELOSTE</a>
- <a class="navlinks"
                 href="<?php echo get_template_directory_uri(); ?>/terms"
                 target="_blank">KÄYTTÖEHDOT</a>
- <a class="navlinks" href="https://www.finnkino.fi/faq" target="_blank">USEIN
                KYSYTYT KYSYMYKSET</a> - <a class="navlinks"
                                            href="http://www.nordiccinemagroup.com/about-nordic-cinema-group/"
                                            target="_blank">NORDIC CINEMA GROUP</a> - <a class="navlinks"
                                                                                         href="https://www.finnkino.fi/feedback/"
                                                                                         target="_blank">PALAUTE JA
                TIEDUSTELUT</a><br/>

            <h1 class="caps" style="font-size:10px;color:#ffffff;">FINNKINO OY - Y-TUNNUS:0647239-1 - OSOITE:
                MANNERHEIMINTIE 113, 00280 HELSINKI - PUHELIN: (09) 131 191 - FAKSI: (09) 853 2183</h1>
        </div>
    </div>

</div>
</body>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>

</html>