<?php
session_start();
get_header();

if($_SESSION['resultCode'] != 2008) {
    header('Location: register');

}
$_SESSION['resultCode'] = 0;
?>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#interests').click(function () {
                checked = $('div.checkbox-group.required :checkbox:checked').length > 0;

                if (!checked) {
                    alert("Valitse ainakin yksi vaihtoehdoista!");
                    return false;
                }

            });
        });

    </script>

    <div class="row fullwidth">
        <div class="col-md-12">
            <div class="checkbox-group required" style="text-align:center">
                <!-- For custom implementation
                <img class="img-responsive finnkino-lab" style="height:150px;width:232px" src="/images/finnkino-lab.png" alt="Finnkino">-->

                <div style="text-align:center;"
                     class="padding80 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 spacetop">
                    <h1 class="caps col-sm-12">Valitse kiinnostuksen kohteet</h1>

                    <form method="POST" action="<?php echo get_template_directory_uri(); ?>/page-form-integration.php">
                        <div class="form-group row"

                        <div class="checkbox" style="text-align:center;">
                            <a style="display:inline-block;text-align:left;">
                                <div class="col-md-12 col-lg-8 col-lg-offset-2 finnkino-yellow" style="">
                                    <label>
                                        <input id="animation" value="1" type="checkbox" name="interests[]"
                                               style="display:none">
                                        <span class="custom-check margin-fix"></span>
                                        <p class="interests">Animaatio<br/></p>
                                    </label>
                                </div>
                                <div class="col-md-12 col-lg-8 col-lg-offset-2 finnkino-yellow">
                                    <label>
                                        <input id="arthouse" value="2" type="checkbox" name="interests[]"
                                               style="display:none">
                                        <span class="custom-check margin-fix"></span>
                                        <p class="interests">Art house</p>
                                    </label>
                                </div>
                                <div class="col-md-12 col-lg-8 col-lg-offset-2 finnkino-yellow">
                                    <label>
                                        <input id="documentary" value="3" type="checkbox" name="interests[]"
                                               style="display:none">
                                        <span class="custom-check margin-fix"></span>
                                        <p class="interests">Dokumentit</p>
                                    </label>
                                </div>
                                <div class="col-md-12 col-lg-8 col-lg-offset-2 finnkino-yellow">
                                    <label>
                                        <input id="drama" value="4" type="checkbox" name="interests[]"
                                               style="display:none">
                                        <span class="custom-check margin-fix"></span>
                                        <p class="interests">Draama</p>
                                    </label>
                                </div>
                                <div class="col-md-12 col-lg-8 col-lg-offset-2 finnkino-yellow">
                                    <label>
                                        <input id="fantasy" value="5" type="checkbox" name="interests[]"
                                               style="display:none">
                                        <span class="custom-check margin-fix"></span>
                                        <p class="interests">Fantasia</p>
                                    </label>
                                </div>
                                <div class="col-md-12 col-lg-8 col-lg-offset-2 finnkino-yellow">
                                    <label>
                                        <input id="thriller" value="6" type="checkbox" name="interests[]"
                                               style="display:none">
                                        <span class="custom-check margin-fix"></span>
                                        <p class="interests">Jännitys</p>
                                    </label>
                                </div>
                                <div class="col-md-12 col-lg-8 col-lg-offset-2 finnkino-yellow">
                                    <label>
                                        <input id="horror" value="7" type="checkbox" name="interests[]"
                                               style="display:none">
                                        <span class="custom-check margin-fix"></span>
                                        <p class="interests">Kauhu</p>
                                    </label>
                                </div>
                                <div class="col-md-12 col-lg-8 col-lg-offset-2 finnkino-yellow">
                                    <label>
                                        <input id="comedy" value="8" type="checkbox" name="interests[]"
                                               style="display:none">
                                        <span class="custom-check margin-fix"></span>
                                        <p class="interests">Komedia</p>
                                    </label>
                                </div>
                                <div class="col-md-12 col-lg-8 col-lg-offset-2 finnkino-yellow">
                                    <label>
                                        <input id="civilisation" value="9" type="checkbox" name="interests[]"
                                               style="display:none">
                                        <span class="custom-check margin-fix"></span>
                                        <p class="interests">Korkeakulttuuri</p>
                                    </label>
                                </div>
                                <div class="col-md-12 col-lg-8 col-lg-offset-2 finnkino-yellow">
                                    <label>
                                        <input id="home" value="10" type="checkbox" name="interests[]"
                                               style="display:none">
                                        <span class="custom-check margin-fix"></span>
                                        <p class="interests">Kotimainen</p>
                                    </label>
                                </div>
                                <div class="col-md-12 col-lg-8 col-lg-offset-2 finnkino-yellow">
                                    <label>
                                        <input id="family" value="11" type="checkbox" name="interests[]"
                                               style="display:none">
                                        <span class="custom-check margin-fix"></span>
                                        <p class="interests">Perhe-elokuvat</p>
                                    </label>
                                </div>
                                <div class="col-md-12 col-lg-8 col-lg-offset-2 finnkino-yellow">
                                    <label>
                                        <input id="popculture" value="12" type="checkbox" name="interests[]"
                                               style="display:none">
                                        <span class="custom-check margin-fix"></span>
                                        <p class="interests">Pop-kulttuuri</p>
                                    </label>
                                </div>
                                <div class="col-md-12 col-lg-8 col-lg-offset-2 finnkino-yellow">
                                    <label>
                                        <input id="romantics" value="13" type="checkbox" name="interests[]"
                                               style="display:none">
                                        <span class="custom-check margin-fix"></span>
                                        <p class="interests">Romantiikka</p>
                                    </label>
                                </div>
                                <div class="col-md-12 col-lg-8 col-lg-offset-2 finnkino-yellow">
                                    <label>
                                        <input id="scifi" value="14" type="checkbox" name="interests[]"
                                               style="display:none">
                                        <span class="custom-check margin-fix"></span>
                                        <p class="interests">Sci-fi</p>
                                    </label>
                                </div>
                                <div class="col-md-12 col-lg-8 col-lg-offset-2 finnkino-yellow">
                                    <label>
                                        <input id="adventure" value="15" type="checkbox" name="interests[]"
                                               style="display:none">
                                        <span class="custom-check margin-fix"></span>
                                        <p class="interests">Seikkailu</p>
                                    </label>
                                </div>
                                <div class="col-md-12 col-lg-8 col-lg-offset-2 finnkino-yellow">
                                    <label>
                                        <input id="action" value="16" type="checkbox" name="interests[]"
                                               style="display:none">
                                        <span class="custom-check margin-fix"></span>
                                        <p class="interests">Toiminta</p>
                                    </label>
                                </div>
                                <div class="col-md-12 col-lg-8 col-lg-offset-2 finnkino-yellow">
                                    <label>
                                        <input id="baby" value="17" type="checkbox" name="interests[]"
                                               style="display:none">
                                        <span class="custom-check margin-fix"></span>
                                        <p class="interests">Vauvakino</p>
                                    </label>
                                </div>

                                <?php
                                $getEmail = $_SESSION['email'];
                                ?>

                                <input id="interests" type="submit" name="movieinterests"
                                       class="btn btn-finnkino col-lg-8 col-lg-offset-2 margin-both-button"
                                       value="Tallenna"
                                       style="display:inline;">


                                <div class="col-md-6 col-lg-6 skip-button" style="">
                                    <a href="..\success" class="btn btn-finnkino  margin-both-button"
                                        style="background-color:#ffffff;display:inline;color:#000000;">&nbsp; Ohita &nbsp;
                                    </a>
                                </div>

                        </div>
                </div>
            </div>
            </form>


        </div>


    </div>


<?php get_footer(); ?>