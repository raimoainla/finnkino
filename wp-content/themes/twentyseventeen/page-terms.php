<? get_header();
?>

<div class="row fullwidth">
    <div class="col-md-12">
        <div style="text-align:center">
            <!-- For custom implementation
            <img class="img-responsive finnkino-lab" style="height:150px;width:232px" src="/images/finnkino-lab.png" alt="Finnkino">-->

            <div style="text-align:left;"
                 class="padding80 col-sm-offset-1 col-sm-10 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
                <h1 class="caps col-sm-12" style="text-align:left !important;margin-left:-18px !important;">
                    Käyttöehdot</h1>

                <div class=" finnkino-yellow">
                    <p class="terms">
                        <?php
                        while (have_posts()) : the_post();

                            get_template_part('template-parts/page/content', 'page');


                        endwhile; // End of the loop.
                        ?>
                    </p>
                </div>

                <div style="text-align:center;">
                    <a href="<?php echo get_template_directory_uri(); ?>/register">
                        <button class="btn btn-finnkino"
                                style="display:inline;margin-top:20px;">Takaisin etusivulle
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    </form>


</div>


</div>
</div>

<div class="col-md-12 padding80" style="text-align:center;padding-bottom:20px !important;">
    <div class="col-md-12"><img class="" width="50" height="50"
                                src="/wordpress/wp-content/themes/twentyseventeen/assets/images/finnkino-logo.png"
                                alt="finnkino-logo"
                                style="margin-bottom:30px;"></div>
    <a class="navlinks" href="https://www.finnkino.fi/" target="_blank">FINNKINO.FI</a> - <a class="navlinks"
                                                                                             href="<?php echo get_template_directory_uri(); ?>/terms"
                                                                                             target="_blank">KÄYTTÖEHDOT</a>
    - <a class="navlinks" href="https://www.finnkino.fi/faq" target="_blank">USEIN
        KYSYTYT KYSYMYKSET</a> - <a class="navlinks"
                                    href="http://www.nordiccinemagroup.com/about-nordic-cinema-group/"
                                    target="_blank">NORDIC CINEMA GROUP</a> - <a class="navlinks"
                                                                                 href="https://www.finnkino.fi/feedback/"
                                                                                 target="_blank">PALAUTE JA
        TIEDUSTELUT</a><br/>

    <h1 class="caps" style="font-size:10px;color:#ffffff;">FINNKINO OY - Y-TUNNUS:0647239-1 - OSOITE:
        MANNERHEIMINTIE 113, 00280 HELSINKI - PUHELIN: (09) 131 191 - FAKSI: (09) 853 2183</h1>
</div>


</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/javascripts/bootstrap.min.js"></script>
</body>
</html>