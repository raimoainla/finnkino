<?php get_header();

?>

<div class="row fullwidth">
    <div class="col-md-12">
        <div style="text-align:center">
            <!-- For custom implementation
            <img class="img-responsive finnkino-lab" style="height:150px;width:232px" src="/images/finnkino-lab.png" alt="Finnkino">-->

            <div style="text-align:center;" class="finnkino-yellow padding80 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 ">
                <p style="font-size:20px;">
                    Syötetty käyttäjä on jo varattu. Tarkista tiedot.
                </p>

                <a href="..\register" target="_blank">
                    <button type="submit" class="btn btn-finnkino"
                            style="display:inline;margin-top:20px;">Takaisin etusivulle
                    </button>
                </a>

            </div>


        </div>


    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/javascripts/bootstrap.min.js"></script>
</body>
</html>
