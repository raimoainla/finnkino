<div class="col-md-10 col-md-offset-1" style="margin-top:20px;">
    <label class="sr-only" required">VVVV *</label>
    <input type="text" name="year" class="fit-21 form-control finnkino-form-custom mb-2 mr-sm-2 mb-sm-0 input"
           id="inputFirstname" placeholder="Vuosi" pattern="[0-9]{4}"
           oninvalid="this.setCustomValidity('Täytä vuosi muodossa VVVV, vain numerot!')"
           oninput="setCustomValidity('')"/>

    <select name="month" class="fit-29 form-control finnkino-form-custom select-dropdown-date" required>
        <option selected>K</option>
        <option value="01">tammikuu</option>
        <option value="02">helmikuu</option>
        <option value="03">maaliskuu</option>
        <option value="04">huhtikuu</option>
        <option value="05">toukokuu</option>
        <option value="06">kesäkuu</option>
        <option value="07">heinäkuu</option>
        <option value="08">elokuu</option>
        <option value="09">syyskuu</option>
        <option value="10">lokakuu</option>
        <option value="11">marraskuu</option>
        <option value="12">joulukuu</option>
    </select>

    <select name="day" class="fit-29 form-control finnkino-form-custom select-dropdown-date" required>
        <option selected>P</option>
        <option>01</option>
        <option>02</option>
        <option>03</option>
        <option>04</option>
        <option>05</option>
        <option>06</option>
        <option>07</option>
        <option>08</option>
        <option>09</option>
        <option>10</option>
        <option>11</option>
        <option>12</option>
        <option>13</option>
        <option>14</option>
        <option>15</option>
        <option>16</option>
        <option>17</option>
        <option>18</option>
        <option>19</option>
        <option>20</option>
        <option>21</option>
        <option>22</option>
        <option>23</option>
        <option>24</option>
        <option>25</option>
        <option>26</option>
        <option>27</option>
        <option>28</option>
        <option>29</option>
        <option>30</option>
        <option>31</option>

    </select>

</div>