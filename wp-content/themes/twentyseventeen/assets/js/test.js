'use strict';

const crypto = require('crypto');
const iso8601 = require('iso8601');
const request = require('request');

const user = 'Finnkino003';
const secret = '44g5G3BPvMa3D7htXqmL';

function getWsseHeader(user, secret) {
    let nonce = crypto.randomBytes(16).toString('hex');
    let timestamp = iso8601.fromDate(new Date());

    let digest = base64Sha1(nonce + timestamp + secret);

    return `UsernameToken Username="${user}", PasswordDigest="${digest}", Nonce="${nonce}", Created="${timestamp}"`
};

function base64Sha1(str) {
    let hexDigest = crypto.createHash('sha1')
        .update(str)
        .digest('hex');

    return new Buffer(hexDigest).toString('base64');
};

request({
    url: 'http://api.emarsys.net/api/v2/settings',
    headers: {
        'Content-Type': 'application/json',
        'X-WSSE': getWsseHeader(user, secret)
    }
}, function(err, response, body) {
    if (err) {
        console.error(err);
    } else {
        console.log('Response Status: ' + response.statusCode);
        console.log('Response Body: ', body);
    }
});