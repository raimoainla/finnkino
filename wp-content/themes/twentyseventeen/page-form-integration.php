<?php
session_start();

class SuiteApiUpdate
{

    private
        $_username,
        $_secret,
        $_suiteApiUrl;

    public function __construct($username, $secret, $suiteApiUrl = 'https://api.emarsys.net/api/v2/')
    {
        $this->_username = $username;
        $this->_secret = $secret;
        $this->_suiteApiUrl = $suiteApiUrl;

    }

    public function send($requestType, $endPoint, $requestBody = '')
    {
        if (!in_array($requestType, array('GET', 'POST', 'PUT', 'DELETE'))) {
            throw new Exception('Send first parameter must be "GET", "POST", "PUT" or "DELETE"');

        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        switch ($requestType) {
            case 'GET':
                curl_setopt($ch, CURLOPT_HTTPGET, 1);
                break;
            case 'POST':

                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
                break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
                break;
            case 'DELETE':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
                break;
        }
        curl_setopt($ch, CURLOPT_HEADER, true);

        $requestUri = $this->_suiteApiUrl . $endPoint;
        curl_setopt($ch, CURLOPT_URL, $requestUri);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        /** Explanation from emarsys.com
         * We add X-WSSE header for authentication.
         * Always use random 'nonce' for increased security.
         * timestamp: the current date/time in UTC format encoded as
         *   an ISO 8601 date string like '2010-12-31T15:30:59+00:00' or '2010-12-31T15:30:59Z'
         * passwordDigest looks sg like 'MDBhOTMwZGE0OTMxMjJlODAyNmE1ZWJhNTdmOTkxOWU4YzNjNWZkMw=='
         */

        $nonce = 'd36e316282959a9ed4c89851497a717f';
        $timestamp = gmdate("c");
        $passwordDigest = base64_encode(sha1($nonce . $timestamp . $this->_secret, false));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'X-WSSE: UsernameToken ' .
                'Username="' . $this->_username . '", ' .
                'PasswordDigest="' . $passwordDigest . '",' .
                'Nonce="' . $nonce . '", ' .
                'Created="' . $timestamp . '"',
                'Content-type: application/json;charset=\"utf-8"',
            )
        );


        if (!curl_exec($ch)) {
            die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
        }


        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        print_r($info);

        curl_close($ch);

        return ($output);


    }
}

$getEmail = $_SESSION['email'];

if (isset($_POST['movieinterests'])) {


    $interests = $_POST['interests'];

    foreach ($interests as $interest => $value) {
        echo $value;
    }

    /* Concatenate array elements (array of interests selected) and make them into acceptable format */

    $string = implode(",", $interests); /*-> $string variable is final output with commas and quotes */



    /* update contact with interests from form */

    $demoUpdate = new SuiteApiUpdate('Finnkino003', '44g5G3BPvMa3D7htXqmL');
    echo $demoUpdate->send('PUT', 'contact', '{"key_id":"3", "3": "' . $getEmail . '", "14756": [' . $string . '] }') . "\n\n";


    /******* test examples ********/
    /*echo " The interests were " . $string . " and email is " . $getEmail;

    /*echo $demoUpdate->send('PUT', 'contact', '{"key_id": "3", "contacts":[{"3":"pasi.raunola@testimaili.fi", "14756":[1,2]}]}') . "\n\n";*/
    /*echo $demo->send('POST', 'contact', '{"3": "' . $_POST["email"] . '", "14756": [1,2,3]}') . "\n\n";*/



    header('Location: success');
    session_unset();
    session_destroy();
    exit();



} else {
    echo "Virhe! Jotakin meni pieleen";
}

/*
if (isset($_POST['submit'])){
    $interests = $_POST['interests'];

    foreach ($interests as $interest => $value) {
        echo "Interests : ".$value."";
    }
}
*/
