<?php
session_start();

class SuiteApi
{

    private
        $_username,
        $_secret,
        $_suiteApiUrl;

    public function __construct($username, $secret, $suiteApiUrl = 'https://api.emarsys.net/api/v2/')
    {
        $this->_username = $username;
        $this->_secret = $secret;
        $this->_suiteApiUrl = $suiteApiUrl;

    }

    public function send($requestType, $endPoint, $requestBody = '')
    {
        if (!in_array($requestType, array('GET', 'POST', 'PUT', 'DELETE'))) {
            throw new Exception('Send first parameter must be "GET", "POST", "PUT" or "DELETE"');

        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        switch ($requestType) {
            case 'GET':
                curl_setopt($ch, CURLOPT_HTTPGET, 1);
                break;
            case 'POST':
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HEADER, false);
                break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
                break;
            case 'DELETE':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
                break;
        }
        curl_setopt($ch, CURLOPT_HEADER, false);


        $requestUri = $this->_suiteApiUrl . $endPoint;
        curl_setopt($ch, CURLOPT_URL, $requestUri);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        /** Explanation from emarsys.com
         * We add X-WSSE header for authentication.
         * Always use random 'nonce' for increased security.
         * timestamp: the current date/time in UTC format encoded as
         *   an ISO 8601 date string like '2010-12-31T15:30:59+00:00' or '2010-12-31T15:30:59Z'
         * passwordDigest looks sg like 'MDBhOTMwZGE0OTMxMjJlODAyNmE1ZWJhNTdmOTkxOWU4YzNjNWZkMw=='
         */

        $nonce = 'd36e316282959a9ed4c89851497a717f';
        $timestamp = gmdate("c");
        $passwordDigest = base64_encode(sha1($nonce . $timestamp . $this->_secret, false));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'X-WSSE: UsernameToken ' .
                'Username="' . $this->_username . '", ' .
                'PasswordDigest="' . $passwordDigest . '",' .
                'Nonce="' . $nonce . '", ' .
                'Created="' . $timestamp . '"',
                'Content-type: application/json;charset=\"utf-8"',
            )
        );


        if (!curl_exec($ch)) {
            die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
        }


        $output = curl_exec($ch);


        /* Retrieving error code from Emarsys API */

        $result = json_decode($output, true);
        $replyCode = $result['replyCode'];

        global $resultCode;
        $resultCode = $replyCode;

        echo " email entered was: " . $_POST["email"] . " and replycode is " . $replyCode . "\n\n";

        return ($output);


        curl_close($ch);
    }


}


/*** If register form is submitted ***/

if (isset($_POST['registration'])) {

    $resultCode = $replyCode;

    $getEmail = $_POST["email"];
    $_SESSION['email'] = $getEmail;

    /*<- concatenating date to correct format for Emarsys ->*/
    $dateFormat = $_POST["year"] . "-" . $_POST["month"] . "-" . $_POST["day"];

    /* Saving email in variable */
    $emailAddress = $_POST["email"];


    /*<- Create a connection with API token ->*/
    $demo = new SuiteApi('Finnkino003', '44g5G3BPvMa3D7htXqmL');

    // checks if date is correct
    list($_POST["year"], $_POST["month"], $_POST["day"]) = explode('-', $dateFormat);
    if (checkdate($_POST["month"], $_POST["day"], $_POST["year"])) {
        $check = 1;
    } else {
        $check = 0;
    }




    // sends data with date

    $email = $_POST['email'];

    // concatenate URI to check contact data first

    $newEmail = str_replace('@', '%40', $email);
    $url = 'contact/?3=' . $newEmail;

    // send contact data to check if user exists

    echo $demo->send("GET", "$url");

    // get and set replyCode

    echo " the returncode was: " . $resultCode;
    $_SESSION['resultCode'] = $resultCode;

    // checks whether date string has enough characters to submit with date and
    //   create a contact with info from page-register.php form

        if ((strlen($dateFormat) > 7) && (strlen($dateFormat) < 11) && ($check == 1)) {

            // if date is correct and valid, enter date

            $dateComplete = $dateFormat;

        } else {

            // if date is not correct or valid, enter null

            $dateComplete = "";
        }


    if ($resultCode == 2008) {

        // Contact is new -> Create profile and change field 35549 for loyal customer -> Yes


        echo $demo->send('POST', 'contact', '{"3": "' . $_POST["email"] . '",
                        "2": "' . $_POST["lname"] . '",
                        "1": "' . $_POST["fname"] . '",
                        "15": "' . $_POST["phone"] . '",
                        "14296": "' . $_POST["location"] . '",
                        "5": "' . $_POST["gender"] . '",
                        "4": "' . $dateComplete . '",
                        "35549":"1"
                        }') . "\n\n";


        header('Location: interests');
        exit();

    } else {

            // Contact already exists, update contact and change field 35549 for loyal customer -> Yes

        echo $demo->send('PUT', 'contact', '{"key_id":"3",
        "3": "' . $_POST["email"] . '",
        "35549":"1"
        }') . "\n\n";

        header('Location: success');
        exit();

    }


}




/**************************** main *****************************/

/*<- Print user request for testing ->
echo $_POST["fname"] . " " . $_POST["lname"] . " " . $_POST["email"] . " " . $_POST["phone"] . " " . $_POST["location"] . " " . $_POST["gender"] . " " . $dateFormat . " Is the data you entered!" . "<br /><br />";*/

/*<- Adding contacts to testlist if needed ->
echo $demo->send('POST', 'contactlist/204430483/add', '{"key_id": "3", "external_ids":["' . $_POST["email"] . '"]}') . "\n\n";*/


/**************************** examples from emarsys site *****************************/

/* to get all fields */
/*echo $demo->send('GET', 'field');*/


/*echo $demo->send('POST', 'contactlist', '{"key_id": "3","name": "Testlist","description": "Test","external_ids": ["test@test.com"]}');*/

/*echo $demo->send('PUT', 'contact', '{"key_id": "3","contacts":[{"3": "herra.testi@test.com","2": "Testi"},{"3": "rouva.testi@test.com","2": "Testi"}]}') . "\n\n";*/
/*echo $demo->send('PUT', 'contact', '{"key_id":"3", "contacts":[{"3": "firstname.lastname@example.com", "2": "Lastname"})}') . "\n\n";*/
/* examples from emarsys: echo $demo->send('GET', 'settings') . "\n\n";
echo $demo->send('POST', 'source/create', '{"name":"RANDOM"}') . "\n\n";
echo $demo->send('PUT', 'contact', '{"key_id": "3","contacts":[{"3": "erik.selvig@example.com","2": "Selvig"},{"3": "ian.boothby@example.com","2": "Boothby"}]}') . "\n\n";
echo $demo->send('DELETE', 'source/1'); */

