<?php
session_start();
get_header();


$headerimage = get_field('headerimage');
$leftblockimage = get_field('leftblockimage');
$rightblockimage = get_field('rightblockimage');
$nosto_1_kuva = get_field('nosto_1_kuva');
$nosto_2_kuva = get_field('nosto_2_kuva');
$nosto_3_kuva = get_field('nosto_3_kuva');
?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>
<div class="row fullwidth">
    <div class="col-md-12">
        <div style="text-align:center">
            <!-- For custom implementation
            <img class="img-responsive finnkino-lab" style="height:150px;width:232px" src="/images/finnkino-lab.png" alt="Finnkino">-->
            <center>
                <!--  For custom implementation
                <div><h1 class="caps mainh1 col-sm-offset-3 col-sm-6">SUURTEN TUNTEIDEN YHTEISÖ</h1></div>-->
                <img class="img-responsive finnkino-header"
                     src="<?php echo $headerimage['url']; ?>"
                     alt="Headerkuva-nainen">
            </center>

            <div style="text-align:center;" class="padding20 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
                <h1 class="caps"><?php the_field('mainheader'); ?></h1>
                <div class="finnkino-yellow">
                    <p><?php the_field('bodytext'); ?></p>
                </div>

                <h1 class="caps" style="margin-top:30px;"><?php the_field('mainheader2'); ?></h1>


            </div>

            <div class="container-fit" style="">

                <div class="col-xs-12 col-sm-6 col-md-6 leftblock">
                    <img class="img-responsive match-height-left" style="float:left;"
                         src="<?php echo $leftblockimage['url']; ?>" width="175"
                         height="173"
                         alt="Lippuviuhka">
                    <h1 class="caps paddingRight" style="text-align:right;">
                        <?php the_field('leftblockheader'); ?>
                    </h1>
                    <div class="finnkino-yellow">
                        <p class="float-nofloat scale-left paddingRight" style="max-width:500px;text-align:right;">
                            <?php the_field('leftblockcontent'); ?>
                        </p>
                    </div>

                </div>


                <div class="col-xs-12 col-sm-6 col-md-6 rightblock">
                    <img class="img-responsive match-height" style="float:right;"
                         src="<?php echo $rightblockimage['url']; ?>" width="121"
                         height="173"
                         alt="Popparirasia">
                    <h1 class="caps paddingLeft" style="color:#232123;text-align:left;">
                        <?php the_field('rightblockheader'); ?>
                    </h1>
                    <p class="paddingLeft" style="color:#232123;text-align:left;">
                        <?php the_field('rightblockcontent'); ?>
                    </p>
                </div>
            </div>

            <div style="text-align:center;padding-top:20px !important;"
                 class="padding80 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
                <h1 class="caps col-sm-12"><?php the_field('registerheader'); ?></h1>

                <form method="POST" action="<?php echo get_template_directory_uri(); ?>/page-integration.php"
                      name="registration">
                    <div class="form-group row">
                        <label class="sr-only" for="inputFirstname" required">Etunimi *</label>
                        <input type="text" name="fname" class="form-control finnkino-form mb-2 mr-sm-2 mb-sm-0 input"
                               id="inputFirstname" pattern="[A-Za-z]{1,50}" placeholder="Etunimi *" required
                               oninvalid="this.setCustomValidity('Täytä etunimesi oikeassa muodossa, vain kirjaimet!')"
                               oninput="setCustomValidity('')"/>

                        <label class="sr-only" for="inputLastname" required>Sukunimi *</label>
                        <input type="text" name="lname" class="form-control finnkino-form mb-2 mr-sm-2 mb-sm-0 input"
                               id="inputLastname" pattern="[A-Za-z]{1,50}" placeholder="Sukunimi *" required
                               oninvalid="this.setCustomValidity('Täytä sukunimesi oikeassa muodossa, vain kirjaimet!')"
                               oninput="setCustomValidity('')"/>

                        <label class="sr-only" for="inputEmail">Sähköpostiosoite *</label>
                        <input type="email" name="email" class="form-control finnkino-form mb-2 mr-sm-2 mb-sm-0 input"
                               id="inputEmail"
                               placeholder="Sähköpostiosoite *" required
                               oninvalid="this.setCustomValidity('Täytä sähköpostiosoitteesi')"
                               oninput="setCustomValidity('')"/>

                        <label class="sr-only" for="inputPhone" for="inputPhone">Puhelinnumero</label>
                        <input type="text" name="phone" class="form-control finnkino-form mb-2 mr-sm-2 mb-sm-0 input"
                               id="inputPhone" pattern="[0-9]{5,20}" placeholder="Puhelinnumero"
                               oninvalid="this.setCustomValidity('Täytä puhelinnumerosi oikeassa muodossa, vain numerot!')"
                               oninput="setCustomValidity('')">

                        <select name="location" id="location" class="form-control finnkino-form select-dropdown-custom"
                                required
                                oninvalid="this.setCustomValidity('Valitse lähin paikkakuntasi')"
                                oninput="setCustomValidity('')">
                            <option value="">Valitse Paikkakunta *</option>
                            <option value="1">Pääkaupunkiseutu</option>
                            <option value="2">Jyväskylä</option>
                            <option value="3">Kuopio</option>
                            <option value="4">Lahti</option>
                            <option value="5">Lappeenranta</option>
                            <option value="6">Oulu</option>
                            <option value="7">Pori</option>
                            <option value="8">Tampere</option>
                            <option value="9">Turku</option>
                        </select>

                        <!-- Load custom date solution -->
                        <?php include 'date.php' ?>


                        <select name="gender" class="form-control finnkino-form select-dropdown-custom"">
                        <option value="">Valitse sukupuoli</option>
                        <option value="1">Mies</option>
                        <option value="2">Nainen</option>
                        </select>


                        <input id="register" name="registration" type="submit" class="btn btn-finnkino"
                               value="Liity jäseneksi"
                               style="display:inline;margin-bottom:10px;">

                        <div class="row finnkino-yellow">
                            <label>
                                <input type="checkbox" class="custom-check" id="check" required style="display:none">
                                <span type="checkbox" id="conditions" class="custom-check" style=""
                                      data-msg-required="my message"></span>
                                <p style="display:inline;font-size:12px;">Olen lukenut ja
                                    hyväksyn
                                    <a href="javascript:void(0);"
                                       NAME="Palvelun ehdot" class="newlink" title="Finnkino Lab palvelun ehdot"
                                       onClick=window.open("<?php echo get_template_directory_uri(); ?>/terms","Finnkino","width=650,height=500,0,status=0,scrollbars=1");>palvelun
                                        ehdot *
                                    </a>
                                </p>

                            </label>
                        </div>


                    </div>
                </form>

                <div
                    class="col-xs-8 col-sm-8 col-md-10 col-lg-8 col-xs-offset-2 col-sm-offset-2 col-md-offset-1 col-lg-offset-2 finnkino-yellow">
                    <p style="font-size:10px;">
                        <?php the_field('terms'); ?>
                    </p>
                </div>
            </div>


        </div>


        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"
             style="background-color:#232123;padding-left:0px;padding-right:0px;">

            <img class="img-responsive" style="width:100%"
                 src="<?php echo $nosto_1_kuva['url']; ?>" width="600"
                 height="399"
                 alt="Kuukauden nosto 1">

            <div class="col-xs-10 col-sm-10 col-sm-offset-1 col-xs-offset-1 height-fix" style="padding-top:20px">
                <h1 class="caps" style="color:#F8C403"><?php the_field('nosto_1_otsikko'); ?></h1>
                <div class="finnkino-yellow">
                    <p class="finnkino-yellow">
                        <?php the_field('nosto_1_sisalto'); ?>

                        <!--Teksti: <b>30.6.2017</b>,<br/> Teksti: <b>4 viikkoa</b> <br/>
                        Tapahtuma <br/><br/>

                        Pelaa Finnkinon ja Hesburgerin Leffapeliä ja voita makeita palkintoja!
                        <br/>

                        Lorem Ipsum Sit Dolor Amet <br />
                        Lorem Ipsum Sit Dolor Amet-->


                    </p>
                </div>

                <div style="text-align:center">


                    <<a href="<?php the_field('nosto_1_linkki'); ?>" target="_blank" alt="kuukauden nosto 1">
                        <button class="btn btn-finnkino"
                                style="display:inline-block;margin-top:30px;margin-bottom:20px;"><?php the_field('nosto_1_nappi'); ?>
                        </button>
                    </a>
                </div>

            </div>

        </div>


        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"
             style="background-color:#F8C403;padding-left:0px;padding-right:0px;">

            <img class="img-responsive" style="width:100%"
                 src="<?php echo $nosto_2_kuva['url']; ?>" width="600"
                 height="399"
                 alt="Kuukauden elokuva">

            <div class="col-xs-10 col-sm-10 col-sm-offset-1 col-xs-offset-1 height-fix" style="padding-top:20px">
                <h1 class="caps" style="color:#000000"><?php the_field('nosto_2_otsikko'); ?></h1>
                <div class="black">
                    <p style="color:#000000">
                        <?php the_field('nosto_2_sisalto'); ?>
                    </p>
                </div>


                <div style="text-align:center">
                    <a href="<?php the_field('nosto_2_linkki'); ?>" target="_blank" alt="Osta liput">
                        <button class="btn btn-finnkino-reverse"
                                style="display:inline-block;margin-top:30px;margin-bottom:20px;"><?php the_field('nosto_2_nappi'); ?>
                        </button>
                    </a>
                </div>
            </div>

        </div>

        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"
             style="background-color:#232123;padding-left:0px;padding-right:0px;">

            <img class="img-responsive" style="width:100%"
                 src="<?php echo $nosto_3_kuva['url']; ?>" width="600"
                 height="399"
                 alt="Kuukauden nosto 2">

            <div class="col-xs-10 col-sm-10 col-sm-offset-1 col-xs-offset-1 height-fix" style="padding-top:20px">
                <h1 class="caps" style="color:#F8C403"><?php the_field('nosto_3_otsikko'); ?></h1>
                <div class="finnkino-yellow">
                    <p class="finnkino-yellow">

                        <?php the_field('nosto_3_sisalto'); ?>
                        <!--
                        Lorem: <b>Ipsum</b>, <br/> Dolor: <b>Amet</b> <br/>
                        Tapahtuma <br/><br/>

                        Vuonna 1986 perustettu Finnkino Oy on Suomen suurin elokuvateatteriketju. Finnkinolla on Suomessa
                        yhdellätoista paikkakunnalla kaikkiaan 16 elokuvateatteria, joissa on yhteensä 104 salia. -->

                    </p>
                </div>


                <div style="text-align:center">
                    <a href="<?php the_field('nosto_3_linkki'); ?>" target="_blank" alt="kuukauden nosto 2">
                        <button class="btn btn-finnkino"
                                style="display:inline-block;margin-top:30px;margin-bottom:20px;"><?php the_field('nosto_3_nappi'); ?>
                        </button>
                    </a>
                </div>

            </div>

        </div>


        <div id='toTop'>
            &nbsp; &#x21E9; &nbsp;
        </div>


        <script>
            $(document).ready(function () {

                $('#toTop:hidden').stop(true, true).fadeIn();

            });

            $(document).scroll(function () {
                if ($(this).scrollTop()) {
                    $('#toTop').stop(true, true).fadeOut();
                } else {
                    $('#toTop:hidden').stop(true, true).fadeIn();
                }
            });

            $(document).ready(function () {
                $('#register').click(function () {
                    check = $("input[type=checkbox]:checked").length;

                    if (!check) {
                        alert("Ehtojen hyväksyminen vaadittu!");
                        return false;
                    }

                });
            });
        </script>

        <div style="margin-top:700px"></div>

        <?php get_footer(); ?>
