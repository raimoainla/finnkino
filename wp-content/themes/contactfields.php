{
"replyCode": 0,
"replyText": "OK",
"data": [
{
"id": 0,
"name": "Interests",
"application_type": "interests",
"string_id": "interests"
},
{
"id": 9,
"name": "Title",
"application_type": "singlechoice",
"string_id": "title"
},
{
"id": 46,
"name": "Salutation",
"application_type": "singlechoice",
"string_id": "salutation"
},
{
"id": 1,
"name": "First Name",
"application_type": "shorttext",
"string_id": "first_name"
},
{
"id": 2,
"name": "Last Name",
"application_type": "shorttext",
"string_id": "last_name"
},
{
"id": 3,
"name": "Email",
"application_type": "longtext",
"string_id": "email"
},
{
"id": 26,
"name": "Preferred email format",
"application_type": "singlechoice",
"string_id": "preferred_email_format"
},
{
"id": 10,
"name": "Address",
"application_type": "longtext",
"string_id": "address"
},
{
"id": 13,
"name": "ZIP Code",
"application_type": "shorttext",
"string_id": "zip_code"
},
{
"id": 11,
"name": "City",
"application_type": "shorttext",
"string_id": "city"
},
{
"id": 12,
"name": "State",
"application_type": "shorttext",
"string_id": "state"
},
{
"id": 14,
"name": "Country",
"application_type": "singlechoice",
"string_id": "country"
},
{
"id": 15,
"name": "Phone",
"application_type": "shorttext",
"string_id": "phone"
},
{
"id": 37,
"name": "Mobile",
"application_type": "shorttext",
"string_id": "mobile"
},
{
"id": 16,
"name": "Fax",
"application_type": "fax",
"string_id": "fax"
},
{
"id": 5,
"name": "Gender",
"application_type": "singlechoice",
"string_id": "gender"
},
{
"id": 4,
"name": "Date of Birth",
"application_type": "birthdate",
"string_id": "birth_date"
},
{
"id": 8,
"name": "Education",
"application_type": "singlechoice",
"string_id": "education"
},
{
"id": 6,
"name": "Marital Status",
"application_type": "singlechoice",
"string_id": "marital_status"
},
{
"id": 38,
"name": "First Name of Partner",
"application_type": "shorttext",
"string_id": "partner_first_name"
},
{
"id": 39,
"name": "Birthdate of Partner",
"application_type": "birthdate",
"string_id": "partner_birth_date"
},
{
"id": 40,
"name": "Anniversary",
"application_type": "date",
"string_id": "anniversary"
},
{
"id": 7,
"name": "Children",
"application_type": "singlechoice",
"string_id": "children"
},
{
"id": 18,
"name": "Company",
"application_type": "longtext",
"string_id": "company_name"
},
{
"id": 41,
"name": "Company Address",
"application_type": "longtext",
"string_id": "company_address"
},
{
"id": 42,
"name": "Zip Code (office)",
"application_type": "shorttext",
"string_id": "company_zip_code"
},
{
"id": 43,
"name": "City (office)",
"application_type": "shorttext",
"string_id": "company_city"
},
{
"id": 44,
"name": "State (office)",
"application_type": "shorttext",
"string_id": "company_state"
},
{
"id": 45,
"name": "Country (office)",
"application_type": "singlechoice",
"string_id": "company_country"
},
{
"id": 21,
"name": "Phone (office)",
"application_type": "shorttext",
"string_id": "company_phone"
},
{
"id": 22,
"name": "Fax (office)",
"application_type": "fax",
"string_id": "company_fax"
},
{
"id": 25,
"name": "URL",
"application_type": "url",
"string_id": "url"
},
{
"id": 17,
"name": "Job Position",
"application_type": "singlechoice",
"string_id": "company_position"
},
{
"id": 19,
"name": "Department",
"application_type": "singlechoice",
"string_id": "company_department"
},
{
"id": 20,
"name": "Industry",
"application_type": "singlechoice",
"string_id": "company_industry"
},
{
"id": 23,
"name": "Number of Employees",
"application_type": "singlechoice",
"string_id": "company_employees"
},
{
"id": 24,
"name": "Annual Revenue (in 000 EUR)",
"application_type": "singlechoice",
"string_id": "company_annual_revenue"
},
{
"id": 14151,
"name": "Training",
"application_type": "singlechoice",
"string_id": ""
},
{
"id": 14154,
"name": "Welcome",
"application_type": "voucher",
"string_id": ""
},
{
"id": 14296,
"name": "Paikkakunta",
"application_type": "singlechoice",
"string_id": ""
},
{
"id": 14756,
"name": "Kiinnostuksen kohteet",
"application_type": "multichoice",
"string_id": ""
},
{
"id": 16747,
"name": "Voucher credit",
"application_type": "numeric",
"string_id": ""
},
{
"id": 16748,
"name": "Voucher expiry date",
"application_type": "date",
"string_id": ""
},
{
"id": 16749,
"name": "Username",
"application_type": "shorttext",
"string_id": ""
},
{
"id": 16962,
"name": "log in name",
"application_type": "shorttext",
"string_id": ""
},
{
"id": 16963,
"name": "Product Name",
"application_type": "longtext",
"string_id": ""
},
{
"id": 16966,
"name": "VoucherPCS",
"application_type": "numeric",
"string_id": ""
},
{
"id": 18145,
"name": "Vastaus",
"application_type": "shorttext",
"string_id": "vastaus"
},
{
"id": 18595,
"name": "Birth month",
"application_type": "numeric",
"string_id": "birth_month"
},
{
"id": 18596,
"name": "Birth year",
"application_type": "numeric",
"string_id": "birth_year"
},
{
"id": 21017,
"name": "Newsletter_Subscribtion_Frosmo",
"application_type": "singlechoice",
"string_id": "newsletter_subscribtion_frosmo"
},
{
"id": 21960,
"name": "Defecting customer",
"application_type": "shorttext",
"string_id": "defecting_customer"
},
{
"id": 22530,
"name": "Ikä",
"application_type": "numeric",
"string_id": "ika"
},
{
"id": 23931,
"name": "Xtesti",
"application_type": "voucher",
"string_id": ""
},
{
"id": 24448,
"name": "Vastauksesi:",
"application_type": "shorttext",
"string_id": ""
},
{
"id": 24517,
"name": "xWelcome popcorn voucher exp. 31.8.16",
"application_type": "voucher",
"string_id": ""
},
{
"id": 24518,
"name": "xWelcome drink voucher exp. 31.8.16",
"application_type": "voucher",
"string_id": ""
},
{
"id": 24519,
"name": "xWelcome 3€ discount voucher exp. 31.8.16",
"application_type": "voucher",
"string_id": ""
},
{
"id": 27174,
"name": "Opiskelijanumero",
"application_type": "numeric",
"string_id": "opiskelijanumero"
},
{
"id": 27175,
"name": "Voimassaoloaika päättyy",
"application_type": "date",
"string_id": "voimassaoloaika_paattyy"
},
{
"id": 27558,
"name": "Student registration",
"application_type": "singlechoice",
"string_id": "student_registration"
},
{
"id": 27710,
"name": "Sehän on...",
"application_type": "singlechoice",
"string_id": ""
},
{
"id": 27897,
"name": "Hän on...",
"application_type": "singlechoice",
"string_id": ""
},
{
"id": 28487,
"name": "ExternalID",
"application_type": "shorttext",
"string_id": "externalid"
},
{
"id": 28605,
"name": "Finnkino- käyttäjätunnus",
"application_type": "shorttext",
"string_id": "finnkino_kayttajatunnus"
},
{
"id": 29230,
"name": "SynttäripopVoucher_syyskuu",
"application_type": "voucher",
"string_id": ""
},
{
"id": 29302,
"name": "SynttäripopVoucher_lokakuu",
"application_type": "voucher",
"string_id": ""
},
{
"id": 29514,
"name": "Welcome 3€ discount voucher exp. 31.3.17",
"application_type": "voucher",
"string_id": ""
},
{
"id": 29515,
"name": "Welcome softdrink voucher exp. 31.3.17",
"application_type": "voucher",
"string_id": ""
},
{
"id": 29516,
"name": "Welcome popcorn voucher exp. 31.3.17",
"application_type": "voucher",
"string_id": ""
},
{
"id": 31148,
"name": "SynttäripopVoucher_marraskuu",
"application_type": "voucher",
"string_id": ""
},
{
"id": 31787,
"name": "Welcome popcorn 2 for 1 voucher exp 31.1.17.",
"application_type": "voucher",
"string_id": ""
},
{
"id": 32108,
"name": "TEST",
"application_type": "voucher",
"string_id": ""
},
{
"id": 32118,
"name": "SynttäripopVoucher_joulukuu",
"application_type": "voucher",
"string_id": ""
},
{
"id": 33029,
"name": "SynttäriPopVoucher_exp280217",
"application_type": "voucher",
"string_id": ""
},
{
"id": 33030,
"name": "WelcomePopVoucher_exp280217",
"application_type": "voucher",
"string_id": ""
},
{
"id": 33031,
"name": "HighSpendCombo_exp280217",
"application_type": "voucher",
"string_id": ""
},
{
"id": 33329,
"name": "Seuralaisen nimi",
"application_type": "shorttext",
"string_id": "seuralaisen_nimi"
},
{
"id": 33330,
"name": "Seuralaisen sähköposti",
"application_type": "shorttext",
"string_id": "seuralaisen_sahkoposti"
},
{
"id": 33669,
"name": "Latest voucher use date",
"application_type": "date",
"string_id": "latest_voucher_use_date"
},
{
"id": 33670,
"name": "Last used voucher products",
"application_type": "longtext",
"string_id": "last_used_voucher_products"
},
{
"id": 33688,
"name": "SynttäriPopVoucher_exp310317",
"application_type": "voucher",
"string_id": ""
},
{
"id": 33689,
"name": "WelcomePopVoucher_exp310317",
"application_type": "voucher",
"string_id": ""
},
{
"id": 34496,
"name": "WelcomePopVoucher_exp300417",
"application_type": "voucher",
"string_id": ""
},
{
"id": 34497,
"name": "SynttäriPopVoucher_exp300417",
"application_type": "voucher",
"string_id": ""
},
{
"id": 35218,
"name": "SynttäriPopVoucher_exp310517",
"application_type": "voucher",
"string_id": ""
},
{
"id": 35219,
"name": "WelcomePopVoucher_exp310517",
"application_type": "voucher",
"string_id": ""
},
{
"id": 35549,
"name": "Loyalty_FinnkinoLab",
"application_type": "singlechoice",
"string_id": "loyalty_finnkinolab"
},
{
"id": 27,
"name": "Avg. length of visit (minutes)",
"application_type": "special",
"string_id": "average_visit_duration"
},
{
"id": 28,
"name": "Page views per day",
"application_type": "special",
"string_id": "pageviews_per_day"
},
{
"id": 29,
"name": "Days since last email sent",
"application_type": "special",
"string_id": "days_since_last_email_sent"
},
{
"id": 31,
"name": "Opt-In",
"application_type": "special",
"string_id": "optin"
},
{
"id": 32,
"name": "User status",
"application_type": "special",
"string_id": "status"
},
{
"id": 33,
"name": "Contact source",
"application_type": "special",
"string_id": "source"
},
{
"id": 34,
"name": "Contact form",
"application_type": "special",
"string_id": "form"
},
{
"id": 35,
"name": "Registration Language",
"application_type": "singlechoice",
"string_id": "registration_language"
},
{
"id": 36,
"name": "Newsletter",
"application_type": "special",
"string_id": "newsletter"
},
{
"id": 47,
"name": "Email valid",
"application_type": "special",
"string_id": "email_valid"
},
{
"id": 48,
"name": "Date of first registration",
"application_type": "special",
"string_id": "registration_date"
}
]
}